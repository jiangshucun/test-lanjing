# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0003_liubingserver_beizhu'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usemen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bk_host_innerip', models.CharField(max_length=30)),
                ('use_men', models.CharField(max_length=30)),
                ('when_created', models.CharField(max_length=100)),
            ],
        ),
    ]
