# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='liubingServer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bk_biz_id', models.CharField(max_length=30)),
                ('bk_host_innerip', models.CharField(max_length=30)),
                ('bk_inst_id', models.CharField(max_length=30)),
                ('bk_inst_name', models.CharField(max_length=30)),
                ('bk_host_name', models.CharField(max_length=30)),
                ('bk_os_name', models.CharField(max_length=30)),
            ],
        ),
        migrations.AlterField(
            model_name='serverinfo',
            name='when_created',
            field=models.CharField(max_length=100),
        ),
    ]
