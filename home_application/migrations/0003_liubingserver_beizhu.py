# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0002_auto_20190107_1237'),
    ]

    operations = [
        migrations.AddField(
            model_name='liubingserver',
            name='beizhu',
            field=models.CharField(default=b'', max_length=500, null=True, blank=True),
        ),
    ]
