# -*- coding: utf-8 -*-
"""
Tencent is pleased to support the open source community by making 蓝鲸智云(BlueKing) available.
Copyright (C) 2017 THL A29 Limited, a Tencent company. All rights reserved.
Licensed under the MIT License (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://opensource.org/licenses/MIT
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
"""
import datetime
import re

from blueking.component.shortcuts import get_client_by_request
from common.mymako import render_mako_context, render_json
from home_application.common2 import search_host_by_ip, fast_execute_script, get_task_ip_log, get_business_by_user, \
    search_biz_by_ip, str_to_datetime
from home_application.models import *


def test(request):
    return render_json({"result": "ok", "username": request.user.username})


def home(request):
    """
    首页
    """
    return render_mako_context(request, '/home_application/home.html')


def dev_guide(request):
    """
    开发指引
    """
    return render_mako_context(request, '/home_application/dev_guide.html')


def contactus(request):
    """
    联系我们
    """
    return render_mako_context(request, '/home_application/contact.html')


def get_biz(request):
    client = get_client_by_request(request)
    result = get_business_by_user(client, request.user.username)
    return render_json(result)


def SubmitInquire(request):
    client = get_client_by_request(request)
    ips = request.POST.get("ips", "")
    biz_id = request.POST.get("biz_id", "")
    ip_list = ips.split("\n")
    # print(biz_id, ips)
    # {"message": "success", "code": 0, "data": {"count": 0, "info": []}, "result": true, "request_id": "2740748d11a14dc6b627dd23347fc8ff"}
    result = search_host_by_ip(client, int(biz_id), ip_list)
    # print(result)
    return_data = []
    if result.get("result"):
        if result.get("data").get("info"):
            for i in result.get("data").get("info"):
                bk_os_name = i.get("host").get("bk_os_name")
                bk_host_name = i.get("host").get("bk_host_name")
                bk_host_innerip = i.get("host").get("bk_host_innerip")
                bk_inst_name = i.get("host").get("bk_cloud_id")[0].get("bk_inst_name")
                bk_inst_id = i.get("host").get("bk_cloud_id")[0].get("bk_inst_id")
                bk_biz_id = i.get("biz")[0].get("bk_biz_id")
                is_monitor = True if Server.objects.filter(
                    bk_host_innerip=i.get("host").get("bk_host_innerip")) else False
                return_data.append(
                    {"bk_os_name": bk_os_name, "bk_host_name": bk_host_name, "bk_host_innerip": bk_host_innerip,
                     "bk_inst_name": bk_inst_name, "bk_inst_id": bk_inst_id, "bk_biz_id": bk_biz_id,
                     'is_monitor': is_monitor})
    # print(return_data)
    return render_json({"result": True, "data": return_data})


def check_info(request):
    bk_inst_id = request.POST.get("bk_inst_id")
    bk_host_innerip = request.POST.get("bk_host_innerip")
    bk_biz_id = request.POST.get("bk_biz_id")
    # print(bk_host_innerip, bk_inst_id, bk_biz_id)
    check_app = {"app_id": bk_biz_id, "ip_list": [{"ip": bk_host_innerip, "bk_cloud_id": bk_inst_id}]}
    client = get_client_by_request(request)
    user_name = "admin"
    execute_account = "root"
    script_content = '''#!/bin/bash
MEMORY=$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')
DISK=$(df -h | awk '$NF=="/"{printf "%s", $5}')
CPU=$(top -bn1 | grep load | awk '{printf "%.2f%%", $(NF-2)}')
DATE=$(date "+%Y-%m-%d %H:%M:%S")
echo -e "$DATE|$MEMORY|$DISK|$CPU"'''
    result = fast_execute_script(check_app, client, user_name, execute_account, script_content, param_content='',
                                 script_timeout=1000)
    if result["result"]:
        script_result = get_task_ip_log(client, check_app["app_id"], result["data"], request.user.username)
        if script_result:
            return_data = format_log_content(script_result[0]["log_content"])
            result["data"] = return_data
    return render_json(result)


def format_log_content(log_content):
    log_result = log_content.strip().split("|")
    one_obj = {
        "createtime": log_result[0],
        "mem_usage": log_result[1].strip("%"),
        "disk_usage": log_result[2].strip("%"),
        "cpu_usage": log_result[3].strip("%"),
    }
    return one_obj


def delete_server(request):
    ip = request.POST.get("bk_host_innerip")
    Server.objects.filter(bk_host_innerip=ip).delete()
    return render_json({"result": True})


def add_server(request):
    bk_inst_id = request.POST.get("bk_inst_id")
    bk_host_innerip = request.POST.get("bk_host_innerip")
    bk_biz_id = request.POST.get("bk_biz_id")
    if Server.objects.filter(bk_host_innerip=bk_host_innerip):
        return render_json({'result': False})
    else:
        Server.objects.create(bk_host_innerip=bk_host_innerip, bk_inst_id=bk_inst_id, bk_biz_id=bk_biz_id)
    return render_json({'result': True})


def get_photo_data(request):
    biz_id = request.POST.get("biz_id")
    ips = request.POST.get("ips")
    now = datetime.datetime.now()
    start = now - datetime.timedelta(hours=1)
    # print(biz_id)
    if ips:
        ips = ips.split("\n")
        server_list = ServerInfo.objects.filter(bk_host_innerip__in=ips, when_created__range=(start, now)).values(
            "bk_host_innerip").distinct()
    elif biz_id:
        server_list = ServerInfo.objects.filter(bk_biz_id=biz_id, when_created__range=(start, now)).values(
            "bk_host_innerip").distinct()
        # print("111")
    else:
        server_list = ServerInfo.objects.filter(when_created__range=(start, now)).values("bk_host_innerip").distinct()
    return_data = []
    # print(server_list)
    for a in server_list:
        id_ip = a.get("bk_host_innerip").replace(".", "_")
        categories = []
        series = []
        series_mem_data = []
        series_dick_data = []
        series_cpu_data = []
        ser_list_ = ServerInfo.objects.filter(bk_host_innerip=a.get("bk_host_innerip"),
                                              when_created__range=(start, now))
        for i in ser_list_:
            categories.append(i.when_created)
            series_mem_data.append(i.mem_usage)
            series_cpu_data.append(i.cpu_usage)
            series_dick_data.append(i.disk_usage)
        series.append({
            "color": "red",
            "name": "内存使用占比,符号（%）",
            "data": series_mem_data
        })
        series.append({
            "color": "blue",
            "name": "CPU使用占比,符号（%）",
            "data": series_cpu_data
        })
        series.append({
            "color": "green",
            "name": "磁盘使用占比,符号（%）",
            "data": series_dick_data
        })
        return_data.append({"id": id_ip, "series": series, "categories": categories})
    # return_data = [{
    #     "id": "123_2_3",
    #     "series": [{
    #         "color": "black",
    #         "name": "dick",
    #         "data": [100, 30, 120, 150, 125, 76, 135, 162, 32, 20, 6, 3]
    #     }, {
    #         "color": "red",
    #         "name": "men",
    #         "data": [200, 180, 190, 150, 125, 76, 133, 122, 32, 20, 6, 3]
    #     }],
    #     "categories": ["07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10", "07:10",
    #                    "07:10", "07:10"]
    # }
    # ]
    # print(return_data)
    return render_json({"result": True, "data": return_data})


def get_ip(request):
    client = get_client_by_request(request)
    biz_id = request.POST.get("biz_id", "")
    result = search_biz_by_ip(client, biz_id)
    return_data = []
    if result.get("result"):
        if result.get("data").get("info"):
            for i in result.get("data").get("info"):
                bk_os_name = i.get("host").get("bk_os_name")
                bk_host_name = i.get("host").get("bk_host_name")
                bk_host_innerip = i.get("host").get("bk_host_innerip")
                bk_inst_name = i.get("host").get("bk_cloud_id")[0].get("bk_inst_name")
                bk_inst_id = i.get("host").get("bk_cloud_id")[0].get("bk_inst_id")
                bk_biz_id = i.get("biz")[0].get("bk_biz_id")
                return_data.append(
                    {"bk_os_name": bk_os_name, "bk_host_name": bk_host_name, "bk_host_innerip": bk_host_innerip,
                     "bk_inst_name": bk_inst_name, "bk_inst_id": bk_inst_id, "bk_biz_id": bk_biz_id})
    # print(return_data)
    return render_json({"result": True, "data": return_data})


def add_server_info(request):
    bk_inst_id = request.POST.get("bk_inst_id", "")
    bk_host_innerip = request.POST.get("bk_host_innerip", "")
    bk_biz_id = request.POST.get("bk_biz_id", "")
    bk_inst_name = request.POST.get("bk_inst_name", "")
    bk_host_name = request.POST.get("bk_host_name", "")
    bk_os_name = request.POST.get("bk_os_name", "")
    if liubingServer.objects.filter(bk_host_innerip=bk_host_innerip):
        return render_json({'result': False})
    else:
        liubingServer.objects.create(bk_host_innerip=bk_host_innerip, bk_inst_id=bk_inst_id, bk_biz_id=bk_biz_id,
                                     bk_inst_name=bk_inst_name, bk_host_name=bk_host_name, bk_os_name=bk_os_name,
                                     beizhu="")
    return render_json({'result': True})


def search_server_info(request):
    bk_host_innerip = request.POST.get("ip", "")
    if bk_host_innerip:
        servers_list = liubingServer.objects.filter(bk_host_innerip=bk_host_innerip)
    else:
        servers_list = liubingServer.objects.all()
    return_data = []
    for i in servers_list:
        return_data.append(
            {"bk_os_name": i.bk_os_name, "bk_host_name": i.bk_host_name, "bk_host_innerip": i.bk_host_innerip,
             "bk_inst_name": i.bk_inst_name, "bk_inst_id": i.bk_inst_id, "bk_biz_id": i.bk_biz_id, 'beizhu': i.beizhu})

    return render_json({"result": True, "data": return_data})


def exam_use(request):
    return render_mako_context(request, '/home_application/exam_use.html')


def delete_server_test(request):
    bk_host_innerip = request.POST.get("bk_host_innerip")
    liubingServer.objects.filter(bk_host_innerip=bk_host_innerip).delete()
    return render_json({"result": True})


# 柱状图
def get_histogram_data(request):
    bk_host_innerip = request.POST.get("bk_host_innerip")
    # print(bk_host_innerip)
    return_data = {
        "series": [{
            "name": "项目a",
            "data": [4.9, 2.0, 32.6, 7.0, 25.6, 76.7, 135.6, 23.2, 162.2, 20.0, 6.4, 3.3]
        }, {
            "name": "项目b",
            "data": [2.6, 5.9, 9.0, 28.7, 182.2, 70.7, 175.6, 26.4, 48.7, 18.8, 6.0, 2.3]
        }],
        "categories": ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"]
    }

    return render_json({"result": True, "data": return_data})


# 饼状图
def get_pie_chart_data(request):
    bk_host_innerip = request.POST.get("bk_host_innerip")
    server = liubingServer.objects.get(bk_host_innerip=bk_host_innerip)
    # print(bk_host_innerip)
    bk_biz_id = server.bk_biz_id
    bk_inst_id = server.bk_inst_id
    check_app = {"app_id": bk_biz_id, "ip_list": [{"ip": bk_host_innerip, "bk_cloud_id": bk_inst_id}]}
    client = get_client_by_request(request)
    user_name = "admin"
    execute_account = "root"
    script_content = '''#!/bin/bash
    free -m'''
    result = fast_execute_script(check_app, client, user_name, execute_account, script_content, param_content='',
                                 script_timeout=1000)
    if result["result"]:
        script_result = get_task_ip_log(client, check_app["app_id"], result["data"], request.user.username)
        if script_result:
            data = [i for i in script_result[0]["log_content"].split("\n")[1].split(" ") if i != '']
            # print(data)
            use_mem = (float(data[2]) / float(data[1])) * 100
            free_mem = 100 - use_mem
            return_data = {
                "title": "内存使用率",
                "series": [{
                    "category": "使用内存",
                    "value": use_mem
                }, {
                    "category": "空闲内存",
                    "value": free_mem
                }]
            }
    return render_json({"result": True, "data": return_data})


# 折线图
def get_line_chart_data(request):
    bk_host_innerip = request.POST.get("bk_host_innerip")
    end_time = request.POST.get("end_time", "")
    start_time = request.POST.get("start_time", "")
    print(end_time, start_time)
    if end_time:
        end_time = re.search(r"(\d{4}-\d{1,2}-\d{1,2}).*?(\d{1,2}:\d{1,2})", end_time)
        print(end_time.group(1) + " " + end_time.group(2))
        now = str_to_datetime(end_time.group(1) + " " + end_time.group(2))
    else:
        now = datetime.datetime.now()
    if start_time:
        start_time = re.search(r"(\d{4}-\d{1,2}-\d{1,2}).*?(\d{1,2}:\d{1,2})", start_time)
        print(start_time.group(1) + " " + start_time.group(2))
        start = str_to_datetime(start_time.group(1) + " " + start_time.group(2))
    else:
        start = now - datetime.timedelta(hours=1)
    server = Usemen.objects.filter(bk_host_innerip=bk_host_innerip, when_created__range=(start, now)).order_by(
        "when_created")
    categories = []
    data = []
    for i in server:
        categories.append(i.when_created)
        data.append(i.use_men)
    return_data = {
        "series": [{
            "color": "#f9ce1d",
            "name": "5分钟平均负载，近一个小时",
            "data": data
        }],
        "categories": categories
    }
    return render_json({"result": True, "data": return_data})


def add_beizhu(request):
    bk_host_innerip = request.POST.get("bk_host_innerip", "")
    beizhu = request.POST.get("beizhu", "")
    liubingServer.objects.filter(bk_host_innerip=bk_host_innerip).update(beizhu=beizhu)
    return render_json({'result': True})


def get_df_info(request):
    bk_host_innerip = request.POST.get("bk_host_innerip")
    server = liubingServer.objects.get(bk_host_innerip=bk_host_innerip)
    # print(bk_host_innerip)
    return_data = []
    bk_biz_id = server.bk_biz_id
    bk_inst_id = server.bk_inst_id
    check_app = {"app_id": bk_biz_id, "ip_list": [{"ip": bk_host_innerip, "bk_cloud_id": bk_inst_id}]}
    client = get_client_by_request(request)
    user_name = "admin"
    execute_account = "root"
    script_content = '''#!/bin/bash
        df -h'''
    result = fast_execute_script(check_app, client, user_name, execute_account, script_content, param_content='',
                                 script_timeout=1000)
    if result["result"]:
        script_result = get_task_ip_log(client, check_app["app_id"], result["data"], request.user.username)
        if script_result:
            logs = script_result[0]["log_content"].split('\n')
            logs = [_l.split(' ') for _l in logs]
            for log in logs[1:]:
                _l_new = [_l for _l in log if _l != '']
                if _l_new and len(_l_new) >= 5:
                    return_data.append({
                        'Filesystem': _l_new[0],
                        'Size': _l_new[1],
                        'Used': _l_new[2],
                        'Avail': _l_new[3],
                        'Use%': _l_new[4],
                        'Mounted': _l_new[5],
                    })
    return render_json({"result": True, "data": return_data})
