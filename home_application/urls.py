# -*- coding: utf-8 -*-
"""
Tencent is pleased to support the open source community by making 蓝鲸智云(BlueKing) available.
Copyright (C) 2017 THL A29 Limited, a Tencent company. All rights reserved.
Licensed under the MIT License (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://opensource.org/licenses/MIT
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
"""

from django.conf.urls import patterns

urlpatterns = patterns(
    'home_application.views',
    (r'^$', 'home'),
    (r'^dev-guide/$', 'dev_guide'),
    (r'^contactus/$', 'contactus'),
    (r'^get_biz$', 'get_biz'),
    (r'^SubmitInquire$', 'SubmitInquire'),
    (r'^check_info$', 'check_info'),
    (r'^add_server$', 'add_server'),
    (r'^delete_server$', 'delete_server'),
    (r'^get_photo_data$', 'get_photo_data'),

    # 刘斌作业
    (r'^get_ip$', 'get_ip'),
    (r'^add_server_info$', 'add_server_info'),
    (r'^search_server_info$', 'search_server_info'),
    (r'^delete_server_test$', 'delete_server_test'),
    (r'^get_histogram_data', 'get_histogram_data'),
    (r'^get_pie_chart_data$', 'get_pie_chart_data'),
    (r'^get_line_chart_data$', 'get_line_chart_data'),
    (r'^add_beizhu$', 'add_beizhu'),
    (r'^get_df_info$', 'get_df_info'),
    (r'^api/test', 'test'),

    # 考试尊卑
    (r'^exam_use/$', 'exam_use'),
)
